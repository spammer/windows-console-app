﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using Newtonsoft.Json;
using System.Runtime.InteropServices;
using System.ComponentModel;
using System.Management;
using System.IO;

namespace windows_console_app
{

  /// <summary>
  /// A utility class to determine a process parent.
  /// </summary>
  [StructLayout(LayoutKind.Sequential)]
  public struct ParentProcessUtilities
  {
    // These members must match PROCESS_BASIC_INFORMATION
    internal IntPtr Reserved1;
    internal IntPtr PebBaseAddress;
    internal IntPtr Reserved2_0;
    internal IntPtr Reserved2_1;
    internal IntPtr UniqueProcessId;
    internal IntPtr InheritedFromUniqueProcessId;

    [DllImport("ntdll.dll")]
    private static extern int NtQueryInformationProcess(IntPtr processHandle, int processInformationClass, ref ParentProcessUtilities processInformation, int processInformationLength, out int returnLength);

    /// <summary>
    /// Gets the parent process of the current process.
    /// </summary>
    /// <returns>An instance of the Process class.</returns>
    public static Process GetParentProcess()
    {
      return GetParentProcess(Process.GetCurrentProcess().Handle);
    }

    /// <summary>
    /// Gets the parent process of specified process.
    /// </summary>
    /// <param name="id">The process id.</param>
    /// <returns>An instance of the Process class.</returns>
    public static Process GetParentProcess(int id)
    {
      Process process = Process.GetProcessById(id);
      return GetParentProcess(process.Handle);
    }

    /// <summary>
    /// Gets the parent process of a specified process.
    /// </summary>
    /// <param name="handle">The process handle.</param>
    /// <returns>An instance of the Process class.</returns>
    public static Process GetParentProcess(IntPtr handle)
    {
      ParentProcessUtilities pbi = new ParentProcessUtilities();
      int returnLength;
      int status = NtQueryInformationProcess(handle, 0, ref pbi, Marshal.SizeOf(pbi), out returnLength);
      if (status != 0)
        throw new Win32Exception(status);

      try
      {
        return Process.GetProcessById(pbi.InheritedFromUniqueProcessId.ToInt32());
      }
      catch (ArgumentException)
      {
        // not found
        return null;
      }
    }
  }



  public static class FocusHandler
  {
    public static string GetCaptionOfWindow(IntPtr hwnd)
    {
      string caption = "";

      StringBuilder windowText = null;
      try
      {
        int max_length = NativeMethods.GetWindowTextLength(hwnd);
        windowText = new StringBuilder("", max_length + 5);
        NativeMethods.GetWindowText(hwnd, windowText, max_length + 2);

        if (!String.IsNullOrEmpty(windowText.ToString()) && !String.IsNullOrWhiteSpace(windowText.ToString()))
          caption = windowText.ToString();
      }
      catch (Exception ex)
      {
        caption = ex.Message;
      }
      finally
      {
        windowText = null;
      }
      return caption;
    }

    public static IEnumerable<Process> GetChildProcesses(this Process process)
    {
        List<Process> children = new List<Process>();
        ManagementObjectSearcher mos = new ManagementObjectSearcher(String.Format("Select * From Win32_Process Where ParentProcessID={0}", process.Id));

        foreach (ManagementObject mo in mos.Get())
        {
            children.Add(Process.GetProcessById(Convert.ToInt32(mo["ProcessID"])));
        }

        return children;
    }

    public static string SwitchToWindow(int processId)
    {
      StringBuilder output = new StringBuilder();
      var process = Process.GetProcessById(processId);
      var parent = ParentProcessUtilities.GetParentProcess(processId);

      if (process != null)
      {
        var handle = process.MainWindowHandle;

        if (parent != null) output.AppendLine("ParentId: " + parent.Id);
        foreach (Process c in GetChildProcesses(process)) {
          output.AppendLine("child: " + c.Id + " " + GetCaptionOfWindow(c.MainWindowHandle) + " " + c.MainWindowTitle);
        }
        IntPtr nextHandle;
        output.AppendLine("\nCaptions:");
        foreach (NativeMethods.GetWindowType wType in (NativeMethods.GetWindowType[])Enum.GetValues(typeof(NativeMethods.GetWindowType))) {
          nextHandle = NativeMethods.GetWindow(handle, wType);
          output.AppendLine(Enum.GetName(typeof(NativeMethods.GetWindowType), wType) + ": " + GetCaptionOfWindow(nextHandle));
        }
        output.AppendLine("\nTitles:");
        foreach (Process p in Process.GetProcesses()) {
          if (p.MainWindowTitle.Contains("(bl=")) {
            output.AppendLine(p.Id + " " + p.MainWindowTitle);
          }
        }
        // Console.WriteLine(NativeMethods.GetWindow(handle, NativeMethods.GetWindowType.GW_HWNDNEXT));
        // Console.WriteLine(NativeMethods.GetWindow(handle, NativeMethods.GetWindowType.GW_HWNDLAST));
        // Console.WriteLine(NativeMethods.GetWindow(handle, NativeMethods.GetWindowType.GW_HWNDFIRST));
        // Console.WriteLine(NativeMethods.GetWindow(handle, NativeMethods.GetWindowType.GW_HWNDPREV));
        // Console.WriteLine(NativeMethods.GetWindow(handle, NativeMethods.GetWindowType.GW_OWNER));
        
        // Console.WriteLine(NativeMethods.GetWindow(handle, NativeMethods.GetWindowType.GW_ENABLEDPOPUP));

        // handle = NativeMethods.GetWindow(handle, NativeMethods.GetWindowType.GW_CHILD);;
        
        // NativeMethods.SetWindowPos(handle,
        //     NativeMethods.HWND_TOP, 0, 0, 0, 0,
        //     NativeMethods.SetWindowPosFlags.SWP_SHOWWINDOW |
        //     NativeMethods.SetWindowPosFlags.SWP_NOSIZE|
        //     NativeMethods.SetWindowPosFlags.SWP_NOMOVE);
        // NativeMethods.ShowWindow(handle, NativeMethods.SW_RESTORE);
        // NativeMethods.SetForegroundWindow(handle);
        NativeMethods.SwitchToThisWindow(handle, true);
      }
      else
      {
        throw new ArgumentException("Unable to find process: " + processId.ToString());
      }

      return output.ToString();
    }
  }
}
